
/* Напишите программу, которая будет считывать из командной строки число (например 41072819) и выводить его в таком виде:
  ***     
 *   * 
 *   * 
 *   * 
 *   * 
 *   * 
  ***  */
public class Program
{
    private static final int WIDTHSCREEN = 10;
    public static void main(String args[])
    {
        int i = 0, j;
        int nums[] = new int[WIDTHSCREEN];
        char arr[] = args[0].toCharArray();
        while(i < arr.length)
        {
            j = 0;
            while(i < arr.length && j < WIDTHSCREEN)
                nums[j++] = Character.digit(arr[i++], 10);
            print(nums, j);
            System.out.println();
        }
    }
    
    static void print(int nums[], int n)
    {
        if(n > 0)
            for(int i = 0; i < 7; i++)
            {
                for(int j = 0; j < n; j++)
                    System.out.print(Nums.strings[nums[j]][i]);
                System.out.println();
            }
    }
}
