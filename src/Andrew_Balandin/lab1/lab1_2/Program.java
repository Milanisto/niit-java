// Реализовать алгоритм вычисления квадратного корня. Взять за основу пример программы. 
// точность - второй аргумент
class Sqrt
{
   double delta = 0.00000001;
   double arg;

   Sqrt(double arg) {
      this.arg = arg;
   }
   double average(double x,double y) {
      return (x + y) / 2.0;
   }
   boolean good(double guess, double imp) {
      return Math.abs(guess - imp) < imp * delta;
   }
   double improve(double guess, double x) {
      return average(guess, x / guess);
   }
   double iter(double guess, double imp, double x) {
      if(good(guess, imp))
         return guess;
      else
         return iter(improve(guess, x), guess, x);
   }
   public double calc(double d) {
      delta = d;
      return iter(1.0, 1.1, arg);
   }
}

class Program
{
   public static void main(String[] args)
   {
      double val = Double.parseDouble(args[0]);
      double d = Double.parseDouble(args[1]);
      Sqrt sqrt=new Sqrt(val);
      double result=sqrt.calc(d);
      System.out.println("Sqrt of "+val+" = "+result);
   }
}
