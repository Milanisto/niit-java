
public class Circle
{
    private double Radius;
    private double Ference;
    private double Area;

    void setRadius(double r)
    {
        Radius = r;
        Ference = 2 * Math.PI * Radius;
        Area = Math.PI * Radius * Radius;
    }
    void setArea(double a)
    {
        Area = a;
        Radius = Math.sqrt(a / Math.PI);
        Ference = 2 * Math.PI * Radius;
    }
    void setFerence(double f)
    {
        Ference = f;
        Radius = Ference / (Math.PI * 2);
        Area = Math.PI * Radius * Radius;
    }
    double getRadius()
    {
        return Radius;
    }
    double getArea()
    {
        return Area;
    }
    double getFerence()
    {
        return Ference;
    }
}