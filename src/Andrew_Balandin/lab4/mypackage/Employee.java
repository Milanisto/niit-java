package mypackage;

/**
 * Created by me on 4/16/17.
 */
public abstract class Employee
{
    public Employee(int id, String name, int workTime, int payment)
    {
        this.id = id;
        this.name = new String(name);
        this.workTime = workTime;
        this.payment = payment;
    }

    protected int id;
    protected String name;
    protected int workTime;
    protected int payment;
    public abstract int calcPayment();
}

interface WorkTime
{
    int calcPaymentForWorkTime();
}

interface Project
{
    int calcPaymentForProject();
}

interface Heading
{
    int calcPaymentForHeading();
}

abstract class Engineer extends Employee implements Project, WorkTime
{

    WorkProject workProject;
    private int percentages;

    Engineer(int id, String name, int workTime, int payment, int percentages)
    {
        super(id, name, workTime, payment);
        this.percentages = percentages;
    }

    public void setWorkProject(WorkProject workProject)
    {
        this.workProject = workProject;
    }

    public int calcPaymentForWorkTime()
    {
        return workTime * payment;
    }

    public int calcPaymentForProject()
    {
        return workProject.price * percentages / 100;
    }

    @Override
    public int calcPayment()
    {
        return calcPaymentForWorkTime() + calcPaymentForProject();
    }
}

abstract class Manager extends Employee implements Project
{
    WorkProject workProject;
    Manager(int id, String name)
    {
        super(id, name, 0, 0);
    }

    public void setWorkProject(WorkProject workProject)
    {
        this.workProject = workProject;
    }
}

abstract class Personal extends Employee implements WorkTime
{
    Personal(int id, String name, int workTime, int payment)
    {
        super(id, name, workTime, payment);
    }

    public int calcPaymentForWorkTime()
    {
        return workTime * payment;
    }

    @Override
    public int calcPayment()
    {
        return calcPaymentForWorkTime();
    }
}