package mypackage;

/**
 * Created by me on 4/21/17.
 */
public class ProjectManager extends Manager implements Heading
{
    final static int paymentForEveryProgrammer = 100;
    public ProjectManager(int id, String name)
    {
        super(id, name);
    }

    public int calcPayment()
    {
        return calcPaymentForHeading() + calcPaymentForProject();
    }

    public int calcPaymentForProject()
    {
        return workProject.price * workProject.percentForManager / 100;
    }

    public int calcPaymentForHeading()
    {
        return workProject.getNumOfEmployees() * paymentForEveryProgrammer;
    }
}
