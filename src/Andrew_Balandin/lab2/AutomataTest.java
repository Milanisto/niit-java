import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by BalandinAS on 28.03.2017.
 */
public class AutomataTest
{

    Automata MyAutomata = new Automata
            (
                    new String[]{"coffee", "tea", "cola"},
                    new int[]{25, 55 , 63},
                    new int[]{1000, 500, 100, 500 ,200, 100},
                    new int[]{2, 5, 1, 10, 100, 50}
            );

    @Test
    public void constructorExceptionsTest()
    {
        try
        {
            Automata WrongAutomata1 = new Automata
                    (
                            new String[]{"coffee", "tea", "cola"},
                            new int[]{25, 55},//only two elements
                            new int[]{1000, 500, 100, 500 ,200, 100},
                            new int[]{2, 5, 1, 10, 100, 50}
                    );
            fail("constructorExceptionsTest failed");
        }
        catch(AutomataException e)
        {
            assertEquals("Constructor Error: Array menu or prices is wrong", e.getMessage());
        }

        try
        {
            Automata WrongAutomata2 = new Automata
                    (
                            new String[]{"coffee", "tea", "cola"},
                            new int[]{25, 55, 33},
                            new int[]{1000, 501, 100, 500 ,200, 100},//501
                            new int[]{2, 5, 1, 10, 100, 50}
                    );
            fail("constructorExceptionsTest failed");
        }
        catch(AutomataException e)
        {
            assertEquals("Constructor Error: Array coins or cash is wrong", e.getMessage());
        }

        try
        {
            Automata WrongAutomata2 = new Automata
                    (
                            new String[]{"coffee", "tea", "cola"},
                            new int[]{25, 55, 33},
                            new int[]{1000, 500, 100, 500, 100},//it has 5 elements
                            new int[]{2, 5, 1, 10, 100, 50}
                    );
            fail("constructorExceptionsTest failed");
        }
        catch(AutomataException e)
        {
            assertEquals("Constructor Error: Array coins or cash is wrong", e.getMessage());
        }

    }

    @Test
    public void getState() throws Exception
    {
        MyAutomata.on();
        assertEquals("WAIT", MyAutomata.getState());
        MyAutomata.off();
    }

    @Test
    //@Ignore
    public void getMenu() throws Exception
    {
        MyAutomata.on();
        assertArrayEquals(
                new String[]
                        {
                                "1) coffee: 25$",
                                "2) tea: 55$",
                                "3) cola: 63$"
                        },
                MyAutomata.getWholeMenu()
        );
        MyAutomata.off();
    }

    @Test
    public void getNominalsOfCoins() throws Exception
    {
        for(int i = 0; i < MyAutomata.getNominalsOfCoins().length - 1; i++)
            if(MyAutomata.getNominalsOfCoins()[i] <= MyAutomata.getNominalsOfCoins()[i + 1])
                fail("getNominalsOfCoins test failed");
        assertArrayEquals(new int[]{100, 50, 10, 5, 2, 1}, MyAutomata.getNominalsOfCoins());
    }

    @Test
    public void on() throws Exception
    {
        MyAutomata.on();
        assertEquals("WAIT", MyAutomata.getState());
        MyAutomata.off();
    }

    @Test
    public void off() throws Exception
    {
        try
        {
            MyAutomata.off();
            fail("off exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("off() State Error", e.getMessage());
        }
        MyAutomata.on();
        MyAutomata.off();
        assertEquals("OFF", MyAutomata.getState());
    }

    @Test
    public void coin() throws Exception
    {//for() with random ?
        MyAutomata.on();
        try
        {
            MyAutomata.cancel();
            fail("coin exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("cancel() State Error", e.getMessage());
        }
        assertTrue(!MyAutomata.coin(8));
        assertEquals("WAIT", MyAutomata.getState());
        assertTrue(MyAutomata.coin(100));
        assertEquals("ACCEPT", MyAutomata.getState());
        MyAutomata.cancel();
        MyAutomata.off();
    }

    @Test
    public void choice() throws Exception
    {
        MyAutomata.on();
        try
        {
            MyAutomata.choice(1);
            fail("choice exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("choice() State Error", e.getMessage());
        }
        MyAutomata.coin(10);
        assertEquals("ACCEPT", MyAutomata.getState());
        assertEquals(0, MyAutomata.choice(1));
        assertEquals(2, MyAutomata.choice(3));
        assertEquals(-1, MyAutomata.choice(5));
        assertEquals("ACCEPT", MyAutomata.getState());
        MyAutomata.cancel();
        MyAutomata.off();
    }

    @Test
    public void check() throws Exception
    {
        MyAutomata.on();

        try
        {
            MyAutomata.check(0);
            fail("check exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("check() State Error", e.getMessage());
        }
        MyAutomata.coin(10);
        assertTrue(!MyAutomata.check(MyAutomata.choice(1)));
        MyAutomata.coin(50);
        assertTrue(MyAutomata.check(MyAutomata.choice(1)));
        assertEquals("CHECK", MyAutomata.getState());
        MyAutomata.cook();
        MyAutomata.change();
        MyAutomata.off();
    }

    @Test
    public void cook() throws Exception
    {
        MyAutomata.on();
        try
        {
            MyAutomata.cook();
            fail("cook exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("cook() State Error", e.getMessage());
        }
        MyAutomata.coin(50);
        MyAutomata.check(MyAutomata.choice(1));
        MyAutomata.cook();
        assertEquals("COOK", MyAutomata.getState());
        MyAutomata.change();
        MyAutomata.off();
    }

    @Test
    public void change() throws Exception
    {
        MyAutomata.on();
        try
        {
            MyAutomata.change();
            fail("change exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("change() State Error", e.getMessage());
        }
        MyAutomata.coin(50);MyAutomata.coin(100);MyAutomata.coin(1);MyAutomata.coin(2);
        MyAutomata.check(MyAutomata.choice(1));
        MyAutomata.cook();
        assertArrayEquals(new int[]{100,0,20,5,2,1}, MyAutomata.change());
        assertEquals("WAIT", MyAutomata.getState());
        MyAutomata.off();
    }

    @Test
    public void cancel() throws Exception
    {
        MyAutomata.on();

        try
        {
            MyAutomata.cancel();
            fail("cancel exception test failed");
        }
        catch (AutomataException e)
        {
            assertEquals("cancel() State Error", e.getMessage());
        }
        MyAutomata.coin(50);
        assertArrayEquals(new int[]{0,50,0,0,0,0}, MyAutomata.cancel());
        assertEquals("WAIT", MyAutomata.getState());
        MyAutomata.off();
    }

}