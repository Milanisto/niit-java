

/**
 * Created by me on 3/26/17.
 */
public class AutomataDemo
{
    public static void main(String args[])
    {
        Automata MyAutomat = new Automata
                (
                    new String[]{"coffee", "tea", "cola"},
                    new int[]{250, 177, 307},
                    new int[]{1000, 500, 0, 0, 20, 100},
                    new int[]{1, 100, 5, 10 ,2, 50}
                );

        MyAutomat.on();

        for(String s : MyAutomat.getWholeMenu())
            System.out.println(s);
        System.out.println();

        System.out.print("You can use: ");
        for(int coin : MyAutomat.getNominalsOfCoins())
            System.out.print(coin + "$ ");
        System.out.println(" coins.");

        MyAutomat.coin(100);
        MyAutomat.coin(100);
        MyAutomat.coin(100);
        System.out.println("Take your money:");
        for(int coin : MyAutomat.cancel())
            System.out.print(coin + " ");
        System.out.println('\n');

        MyAutomat.coin(100);
        MyAutomat.coin(100);
        MyAutomat.coin(100);
        MyAutomat.coin(50);
        int p;
        if(MyAutomat.check(p = MyAutomat.choice(3)))
        {
            MyAutomat.cook();
            System.out.println("One moment! Take your change:");
            for(int coin : MyAutomat.change())
                System.out.print(coin + " ");
            System.out.println();
            System.out.println("Take your " + MyAutomat.getMenu()[p] + "!");
        }
        else
        {
            System.out.println("Not enough money!");
        }

        //System.out.println(MyAutomat.getState());
        MyAutomat.off();

    }
}
