package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class CleanerTest extends TestCase {
    public void testCalcPayment() throws Exception {
        Cleaner cleaner = new Cleaner(1030,"Лисицына Л.Л.","Cleaner",120);
        cleaner.setWorkTime(8);
        assertEquals(960.0,cleaner.calcPayment());
    }

}