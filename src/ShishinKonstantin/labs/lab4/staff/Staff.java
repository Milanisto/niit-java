package kostya;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Staff
{
    private int numEmployees=0;                 //количество сотрудников
    private Employee[] employees=null;          //ссылка на массив сотрудников
    private int numProjects=0;                  //количество проектов

    Staff(){
        //вычисление количества сотрудников в файле
        try (FileReader fr = new FileReader("staff.txt"))	{
            BufferedReader br = new BufferedReader(fr);
            while (br.readLine() != null) {
                numEmployees++;
            }
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
        catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

        //вычисление количества проектов в файле
        try (FileReader fr = new FileReader("projects.txt"))	{
            BufferedReader br = new BufferedReader(fr);
            while (br.readLine() != null) {
                numProjects++;
            }
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
        catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

        //создание массивов сведений о сотрудниках
        int[] ids = new int[numEmployees];                              //id сотрудников
        String[] names = new String[numEmployees];                      //ФИО сотрудников
        String[] positions = new String[numEmployees];                  //должности сотрудников
        int[] bases = new int[numEmployees];                            //ставки сотрудников
        String[] currentProjects = new String[numEmployees];            //названия проектов, в которых участвуют сотрудники
        int[] numWrittenLinesOfCode = new int[numEmployees];            //количество строк кода, написанных сотрудником

        //заполнение массивов сведений о сотрудниках данными из файла
        try (FileReader fr = new FileReader("staff.txt")) {
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < numEmployees; i++) {
                String[] strEmployees = br.readLine().split(",");
                ids[i] = Integer.parseInt(strEmployees[0]);
                names[i] = strEmployees[1];
                positions[i] = strEmployees[2];
                bases[i] = Integer.parseInt(strEmployees[3]);
                currentProjects[i] = strEmployees[4];
                numWrittenLinesOfCode[i] = Integer.parseInt(strEmployees[5]);
            }
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
        catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

        //создание массивов сведений о проектах
        String[] projectsNames = new String[numProjects];               //названия проектов
        int[] projectsCosts = new int[numProjects];                     //стоимости проектов
        int[] projectsNumLinesOfCode = new int[numProjects];            //количество строк в проектах

        //заполнение массивов сведений о проектах данными из файла
        try (FileReader fr = new FileReader("projects.txt")) {
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < numProjects; i++) {
                String[] strProjects = br.readLine().split(",");
                projectsNames[i] = strProjects[0];
                projectsCosts[i] = Integer.parseInt(strProjects[1]);
                projectsNumLinesOfCode[i] = Integer.parseInt(strProjects[2]);

                //System.out.println(projectsNames[i]+" "+projectsCosts[i]+" "+projectsNumLinesOfCode[i]);
            }
        }
        catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
        catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

        //создание массива сотрудников
        employees = new Employee[numEmployees];

        //заполнение массива сотрудников
        for(int i=0; i<numEmployees; i++){

            int allProjectsCost = 0;                //стоимость всех проектов
            int projectCost = 0;                    //стоимость проекта
            int numLinesOfCodeInProject = 0;        //количество строк кода в проекте
            int numSubordinates = 0;                //количество подчиненных
            int numManagersInProject = 0;           //количество менеджеров у проекта
            int numTestersInProject = 0;            //количество тестеров, участвующих в проекте

            switch (positions[i]) {

                case "SeniorManager":
                    allProjectsCost = calcAllProjectsCost(projectsCosts);
                    numSubordinates = calcNumSubordinates(positions);
                    employees[i] = new SeniorManager(ids[i], names[i], positions[i], projectCost, allProjectsCost, numManagersInProject, numSubordinates);
                    break;

                case "ProjectManager":
                    projectCost = calcProjectCost(currentProjects[i], projectsNames, projectsCosts);
                    numManagersInProject = calcNumManagersInProject(positions, currentProjects, currentProjects[i]);
                    numSubordinates = calcNumSubordinates("Manager", "TeamLeader", positions, currentProjects, currentProjects[i]);
                    employees[i] = new ProjectManager(ids[i], names[i], positions[i], projectCost, numManagersInProject, numSubordinates);
                    break;

                case "Manager":
                    projectCost = calcProjectCost(currentProjects[i], projectsNames, projectsCosts);
                    numManagersInProject = calcNumManagersInProject(positions, currentProjects, currentProjects[i]);
                    employees[i] = new Manager(ids[i], names[i], positions[i], projectCost, numManagersInProject);
                    break;

                case "TeamLeader":
                    projectCost = calcProjectCost(currentProjects[i], projectsNames, projectsCosts);
                    numLinesOfCodeInProject = calcNumLinesOfCodeInProject(currentProjects[i], projectsNames, projectsNumLinesOfCode);
                    numSubordinates = calcNumSubordinates("", "", positions, currentProjects, currentProjects[i]);
                    employees[i] = new TeamLeader(ids[i], names[i], positions[i], bases[i], projectCost, numLinesOfCodeInProject, numWrittenLinesOfCode[i], numSubordinates);
                    break;

                case "Programmer":
                    projectCost = calcProjectCost(currentProjects[i], projectsNames, projectsCosts);
                    numLinesOfCodeInProject = calcNumLinesOfCodeInProject(currentProjects[i], projectsNames, projectsNumLinesOfCode);
                    employees[i] = new Programmer(ids[i], names[i], positions[i], bases[i], projectCost, numLinesOfCodeInProject, numWrittenLinesOfCode[i]);
                    break;

                case "Tester":
                    projectCost = calcProjectCost(currentProjects[i], projectsNames, projectsCosts);
                    numLinesOfCodeInProject = calcNumLinesOfCodeInProject(currentProjects[i], projectsNames, projectsNumLinesOfCode);
                    numTestersInProject = calcNumTestersInProject(positions, currentProjects, currentProjects[i]);
                    employees[i] = new Tester(ids[i], names[i], positions[i], bases[i], projectCost, numLinesOfCodeInProject, numTestersInProject);
                    break;

                case "Cleaner":
                    employees[i] = new Cleaner(ids[i], names[i], positions[i], bases[i]);
                    break;

                case "Driver":
                    employees[i] = new Driver(ids[i], names[i], positions[i], bases[i]);
                    break;
            }
        }
    }

    //получение количества сотрудников
    public int getNumEmployees(){
        return numEmployees;
    }

    //вычисление стоимости всех проектов
    public int calcAllProjectsCost(int[] projectsCosts){
        int allProjectsCost = 0;
        for(int j=0; j<numProjects; j++) allProjectsCost += projectsCosts[j];
        return allProjectsCost;
    }

    //вычисление стоимости проекта
    public int calcProjectCost (String currentProject, String[] projectsNames, int[] projectCosts){
        int projectCost = 0;
        for(int j=0; j<numProjects; j++){
            if(currentProject.equals(projectsNames[j])) projectCost = projectCosts[j];
        }
        return projectCost;
    }

    //вычисление количества строк кода в проекте
    public int calcNumLinesOfCodeInProject(String currentProject, String[] projectsNames, int[] projectsNumLinesOfCode){
        int numLinesOfCodeInProject = 0;
        for(int j=0; j<numProjects; j++){
            if(currentProject.equals(projectsNames[j])) {
                numLinesOfCodeInProject = projectsNumLinesOfCode[j];
            }
        }
        return numLinesOfCodeInProject;
    }

    //вычисление количества менеджеров у проекта
    public int calcNumManagersInProject (String[] positions, String[] currentProjects, String currentProject){
        int numManagersInProject = 0;
        for(int j=0; j<numEmployees; j++){
            if(positions[j].equals("Manager") && currentProjects[j].equals(currentProject)) numManagersInProject++;
        }
        return numManagersInProject;
    }

    //вычисление количества тестеров, участвующих в проекте
    public int calcNumTestersInProject(String[] positions, String[] currentProjects, String currentProject){
        int numTestersInProject = 0;
        for(int j=0; j<numEmployees; j++){
            if(positions[j].equals("Tester") && currentProjects[j].equals(currentProject)) numTestersInProject++;
        }
        return numTestersInProject;
    }

    //вычисление количества подчиненных у руководителя направления
    public int calcNumSubordinates(String[] positions){
        int numSubordinates = 0;
        for(int j=0; j<numEmployees; j++) {
            if (positions[j].equals("ProjectManager") || positions[j].equals("Manager") ||
                    positions[j].equals("TeamLeader") || positions[j].equals("Programmer") ||
                    positions[j].equals("Tester")) {
                numSubordinates++;
            }
        }
        return numSubordinates;
    }

    //вычисление количества подчиненных у проектного менеджера и ведущего программиста
    public int calcNumSubordinates(String manager, String teamLeader, String[] positions, String[] currentProjects, String currentProject){
        int numSubordinates = 0;
        for(int j=0; j<numEmployees; j++) {
            if ((positions[j].equals(manager) && currentProjects[j].equals(currentProject)) ||
                    (positions[j].equals(teamLeader) && currentProjects[j].equals(currentProject)) ||
                    (positions[j].equals("Programmer") && currentProjects[j].equals(currentProject)) ||
                    (positions[j].equals("Tester") && currentProjects[j].equals(currentProject))) {
                numSubordinates++;
            }
        }
        return numSubordinates;
    }

    public static void main(String[] args) {
        Staff staff = new Staff();
        System.out.printf("%-20s%-25s%-20s%-10s%n", "Должность", "ФИО", "Отр. время", "Зарплата");
        System.out.println("---------------------------------------------------------------------------");
        for(int i=0; i<staff.getNumEmployees(); i++){
            staff.employees[i].setWorkTime(160);
            staff.employees[i].calcPayment();
            staff.employees[i].printEmployeesInfo();
        }
    }
}
