package kostya;

//менеджер. Оплату получает из денег проекта, которым руководит.
public class Manager extends Employee implements Project{
    private int projectCost = 0;                    //стоимость проекта
    private int numManagersInProject = 0;           //количество менеджеров у проекта

    public Manager(int id, String name, String position, int projectCost, int numManagersInProject){
        super(id, name, position);
        this.projectCost = projectCost;
        this.numManagersInProject = numManagersInProject;
    }

    //получение стоимости проекта
    public int getProjectCost(){
        return projectCost;
    }

    //получение количества менеджеров
    public int getNumManagers(){
        return numManagersInProject;
    }

    //расчет оплаты исходя из участия в проекте (10% от стоимости проекта делятся между менеджерами поровну)
    @Override

    public double calcPaymentForProject() {
        return getProjectCost() * 0.1 / numManagersInProject;
    }

    //расчет зароботной платы
    @Override
    public double calcPayment() {
        setPayment(calcPaymentForProject());
        return getPayment();
    }
}
