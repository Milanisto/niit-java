package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class TesterTest extends TestCase {
    public void testCalcPaymentForWorkTime() throws Exception {
        Tester tester = new Tester(1021,"Баранов Б.Б.","Tester",300,1000000,0,2);
        tester.setWorkTime(8);
        assertEquals(2400.0,tester.calcPaymentForWorkTime());
    }

    public void testCalcPaymentForProject() throws Exception {
        Tester tester = new Tester(1021,"Баранов Б.Б.","Tester",300,1000000,0,2);
        assertEquals(25000.0,tester.calcPaymentForProject());
    }

    public void testCalcPayment() throws Exception {
        Tester tester = new Tester(1021,"Баранов Б.Б.","Tester",300,1000000,0,2);
        tester.setWorkTime(8);
        assertEquals(27400.0,tester.calcPayment());
    }

}