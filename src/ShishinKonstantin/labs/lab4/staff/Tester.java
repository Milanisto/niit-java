package kostya;

//инженер по тестированию.
public class Tester extends Engineer{
    private int numTestersInProject = 0;        //количество тестеров, участвующих в проекте

    public Tester(int id, String name, String position, int base, int projectCost, int numLinesOfCodeInProject, int numTestersInProject){
        super(id, name, position, base, projectCost, numLinesOfCodeInProject);
        this.numTestersInProject = numTestersInProject;
    }

    //расчет оплаты исходя из участия в проекте (5% от стоимости выполняемого проекта делятся между тестерами поровну)
    @Override
    public double calcPaymentForProject() {
        return getProjectCost() * 0.05 / numTestersInProject;
    }
}
