package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class ProjectManagerTest extends TestCase {
    public void testCalcPaymentForProject() throws Exception {
        ProjectManager projectManager = new ProjectManager(1002,"Петров П.П.","ProjectManager",1000000,0,10);
        assertEquals(300000.0,projectManager.calcPaymentForProject());
    }

    public void testCalcPaymentForHeading() throws Exception {
        ProjectManager projectManager = new ProjectManager(1002,"Петров П.П.","ProjectManager",1000000,0,10);
        assertEquals(100000.0,projectManager.calcPaymentForHeading());
    }

    public void testCalcPayment() throws Exception {
        ProjectManager projectManager = new ProjectManager(1002,"Петров П.П.","ProjectManager",1000000,0,10);
        assertEquals(400000.0,projectManager.calcPayment());
    }

}