package kostya;


import java.io.*;
import java.util.Locale;


//класс Деканат
public class Dekanat
{
	private Group[] groups;										//массив групп
	private Student[] students = new Student[150];				//массив студентов
	private int numGroups=0;									//количество групп в файле
	private int numStudents=0;									//количество студентов в файле

	//создание деканата
	public Dekanat()
	{
		//вычисление количества групп в файле
		try (FileReader fr = new FileReader("groups.txt"))	{
			BufferedReader br = new BufferedReader(fr);
			while (br.readLine() != null) {
				numGroups++;
			}
		}
		catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
		catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

		System.out.println("Количество групп: " + numGroups);

		//создание групп на основе данных из файла
		String[] strGroups = new String[numGroups];
		groups = new Group[numGroups];
		try (FileReader fr = new FileReader("groups.txt")){
			BufferedReader br = new BufferedReader(fr);
			for(int i=0; i<numGroups; i++){
				strGroups[i]=br.readLine();
				groups[i] = new Group(strGroups[i]);
			}
		}
		catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
		catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

		//вычисление количества студентов в файле
		try (FileReader fr = new FileReader("students.txt")) {
			BufferedReader br = new BufferedReader(fr);
			while (br.readLine() != null) {
				numStudents++;
			}
		}
		catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
		catch(IOException ioex){System.out.println("Ошибка чтения файла!");}

		System.out.println("Количество студентов: "+numStudents);

		//создание студентов на основе данных из файла и назначение им групп
		String[] strStudents = new String[numStudents];
		try (FileReader fr = new FileReader("students.txt")){
			BufferedReader br = new BufferedReader(fr);
			for(int i=0; i<numStudents; i++){
				strStudents[i]=br.readLine();
				String[] strStudentsArr = strStudents[i].split(",");
				students[i] = new Student(Integer.parseInt(strStudentsArr[0]), strStudentsArr[1]);

				try{
					for(int j=0; j<numGroups; j++){
						if(strStudentsArr[2].equals(strGroups[j])){
							students[i].setGroup(groups[j]);			//назначение группы студенту
							groups[j].addStudentInGroup(students[i]);	//добавление студента в группу
						}
					}
				}
				catch(ArrayIndexOutOfBoundsException aioex){System.out.println("В каждой группе должно быть не больше 30 студентов!");}
			}
		}
		catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
		catch(IOException ioex){System.out.println("Ошибка чтения файла!");}
	}

	//возвращение массива студентов (для теста)
	public Student[] getStudents(){
		return students;
	}

	//возвращение количества студентов (для теста)
	public int getNumStudents(){
		return numStudents;
	}

	//возвращение массива групп (для теста)
	public Group[] getGroups(){
		return groups;
	}

	//добавление случайных оценок студентам
	private void addMarkStudents() {
		try {
			for (int i = 0; i < numStudents; i++) students[i].addMark();
		}
		catch(ArrayIndexOutOfBoundsException aioex){System.out.println("Можно поставить не больше 10 оценок!");}
	}

	//вычисление среднего балла по студентам
	private void calcResultsStudents() {
		for (int i = 0; i < numStudents; i++) {
			System.out.print(students[i].getId() + ", " + students[i].getFio() + ", группа " + students[i].getGroup().getTitle() + ", средний балл - ");
			System.out.printf(Locale.ENGLISH, "%.2f%n", students[i].calcAverageMarkStudent());
		}
	}

	//вычисление среднего балла по группам
	private void calcResultsGroups() {
		for(int i=0; i<numGroups; i++){
			System.out.print("Группа "+groups[i].getTitle()+", средний балл - ");
			System.out.printf(Locale.ENGLISH,"%.2f%n",groups[i].calcAverageMarkGroup());
		}
	}

	//инициация выборов старост в группах
	private void beginChoiceHead() {
		for(int i=0; i<numGroups; i++) groups[i].choiceHead();
	}

	//поиск студента по ID
	private void findStudent(int id) {
		Student foundStudent=null;
		String strMassage="";
		for(int i=0; i<numStudents; i++){
			if(students[i].getId()==id){
				foundStudent = students[i];
				strMassage = "Найден студент: "+foundStudent.getId()+", "+foundStudent.getFio()+", группа "+foundStudent.getGroup().getTitle();
				break;
			}
			else strMassage = "Студент c ID "+id+" не найден!";
		}
		System.out.println(strMassage);
	}

	//поиск студента по ФИО
	private void findStudent(String fio) {
		Student foundStudent=null;
		String strMassage="";
		for(int i=0; i<numStudents; i++){
			if(students[i].getFio().equals(fio)){
				foundStudent = students[i];
				strMassage = "Найден студент: "+foundStudent.getId()+", "+foundStudent.getFio()+", группа "+foundStudent.getGroup().getTitle();
				break;
			}
			else strMassage = "Студент "+fio+" не найден!";
		}
		System.out.println(strMassage);
	}
	
	//перевод студентов из группы в группу
	private void transferStudent(String fio, String newGroup){
		for(int i=0; i<numStudents; i++){
			if(students[i].getFio().equals(fio)){
				for(int j=0; j<numGroups; j++){
					if(groups[j].equals(students[i].getGroup())) {
						groups[j].deleteStudentFromGroup(students[i]);	//удаление студента из группы
					}
				}
				for(int j=0; j<numGroups; j++){
					if(groups[j].getTitle().equals(newGroup)){
						students[i].setGroup(groups[j]);				//назначение новой группы студенту
						groups[j].addStudentInGroup(students[i]);		//добавление студента в новую группу
					}
				}
			}
		}
	}

	//прием студента
	private void addStudent(String fio) {
		try{
			int maxID=0;
			for(int i=0; i<numStudents; i++){
				if(students[i].getId()>maxID) maxID=students[i].getId();	//нахождение максимального ID студента
			}
			students[numStudents] = new Student(maxID+1, fio);			//создание нового студента
			//( Math.random() * (max+1 - min) ) + min
			int randomGroup =(int) ( Math.random() * (numGroups));			//генерирование случайной группы от 0 до numGroups-1
			students[numStudents].setGroup(groups[randomGroup]);			//назначение случайной группы студенту
			groups[randomGroup].addStudentInGroup(students[numStudents]);	//добавление студента в случайную группу
			numStudents++;

		}
		catch(ArrayIndexOutOfBoundsException aioex){System.out.println("В деканате может быть не больше 150 студентов!");}
	}

	//отчисление студентов за неуспеваемость
	private void deleteWeakStudents() {
		Student[] studentsAfterDelete = new Student[150];
		int num=0;	//количество неотчисленных студентов
		for (int i=0; i<numStudents; i++){
			if(students[i].calcAverageMarkStudent()>=3){
				studentsAfterDelete[num]=students[i];							//копирование неотчисленных студентов во временный массив
				num++;
			}
			else {
				students[i].getGroup().deleteStudentFromGroup(students[i]);		//удаление студента из группы
			}
		}
		numStudents=num;
		for(int i=0; i<numStudents; i++) students[i]=studentsAfterDelete[i];
	}
	
	//обновление данных в файле студентов
	private void updateDataInFileStudents() {
		try(FileWriter fw = new FileWriter("students.txt",false)) {
			for(int i=0; i<numStudents; i++){
				fw.write(students[i].getId()+","+students[i].getFio()+","+students[i].getGroup().getTitle()+"\r\n");
			}
			System.out.println("Данные в файле студентов обновлены!");
		}
		catch(FileNotFoundException fnfex){System.out.println("Файл не найден!");}
		catch(IOException ioex){System.out.println("Ошибка записи файла!");}
	}
	
	public static void main(String[] args)
	{
		Dekanat dekanat = new Dekanat();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		dekanat.addMarkStudents();
		System.out.println("Вычисление среднего балла по студентам:");
		dekanat.calcResultsStudents();
		System.out.println("Вычисление среднего балла по группам:");
		dekanat.calcResultsGroups();
		System.out.println("Выбор старост в группах:");
		dekanat.beginChoiceHead();
		dekanat.findStudent(1001);
		dekanat.findStudent("Поисков П.П.");
		dekanat.transferStudent("Петров П.П.","16-В");
		System.out.println("Вычисление среднего балла по студентам после перевода студента в другую группу:");
		dekanat.calcResultsStudents();
		System.out.println("Вычисление среднего балла по группам после перевода студента в другую группу:");
		dekanat.calcResultsGroups();
		dekanat.addStudent("Новиков Н.Н.");
		dekanat.addMarkStudents();
		System.out.println("Вычисление среднего балла по студентам после добавления нового студента:");
		dekanat.calcResultsStudents();
		System.out.println("Вычисление среднего балла по группам после добавления нового студента:");
		dekanat.calcResultsGroups();
		dekanat.deleteWeakStudents();
		System.out.println("Вычисление среднего балла по студентам после отчисления студентов за неуспеваемость:");
		dekanat.calcResultsStudents();
		System.out.println("Вычисление среднего балла по группам после отчисления студентов за неуспеваемость:");
		dekanat.calcResultsGroups();
		dekanat.updateDataInFileStudents();
	}
}
