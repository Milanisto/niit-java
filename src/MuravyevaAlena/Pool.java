
public class Pool {
   static int priceFence=2000;
   static int priceCoating=1000;
   public static void main(String[]args){
      Pool sr = new Pool();
      Circles fence=new Circles(3,18.84955592153876,28.27);
      Circles track =new Circles(4,25.132741228718345,50.27);
      System.out.printf("%5.3f���.\n",sr.calcFence(track));
      System.out.printf("%5.3f���.\n",sr.calcTrack(track,fence));
   }
   double calcFence(Circles _track){
      double costFence;
      costFence = _track.getFerence()*priceFence;
      return costFence;
   }
   double calcTrack(Circles _track, Circles _earth){
      double areaTrack;
      areaTrack= (_track.getArea()- _earth.getArea())*priceCoating;
      return areaTrack;
   }
}
class Circles {
   private final double pi=3.141592;
   private double radius;
   private double ference;
   private double area;
   
   public Circles(double r, double f,double a){
      
      radius=r;
      ference=f;
      area=a;
   }
   public void SetNewRadius(double r){
      radius=r;
      area=pi*(2*radius);
      ference=2*pi*radius;
   }
   public void SetNewFerence(double f){
      ference=f;
      radius=ference/(2*pi);
      area=pi*(2*radius);
   }
   public void SetNewArea(double a){
      area=a;
      radius=area/pi;
      ference=2*pi*radius;
   }
   public double getFerence() {
      return ference;
   }
   public double getArea() {
      return area;
   }
}


