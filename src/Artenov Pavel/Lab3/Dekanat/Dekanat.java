package Dekanat2;
/*
Разработать класс Dekanat
Перечень полей:
Students-массив студентов
Groups-массив групп
Обеспечить класс следующими методами:
-создание студентов на основе данных из файла
-создание групп на основе данных из файла
-добавление случайных оценок студентам
-накопление статистики по успеваемости студентов и групп
-перевод студентов из группы в группу
-отчисление студентов за неуспеваемость
-сохранение обновленных данных в файлах
-инициация выборов старост в группах
-вывод данных на консоль
 */
import java.io.*;

public class Dekanat {
    private int num=40;
    private Student[] studentArr = new Student[num];
    private Group[] groupArr = new Group[3];

    //создание студентов из файла
    public void addStudent (String list){
        int i = 0;
        String fio;
        try {
            File fileStudent = new File(list);
            FileReader studentReader = new FileReader(fileStudent);
            BufferedReader bStudentReader = new BufferedReader(studentReader);
            while ((fio = bStudentReader.readLine()) != null){
                studentArr[i] = new Student(fio,i+1);
                i++;
            }
            studentReader.close();
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("Error,file 'student' not found.");
        }
        catch (IOException ioe) {
            System.out.println("Error,can't read file.");
        }
    }
    //создание групп из файла
    public void addGroup (String group) {
        int i = 0;
        String groupName;
        try {
            File fileGroup = new File(group);
            FileReader groupReader = new FileReader(fileGroup);
            BufferedReader bGroupReader = new BufferedReader(groupReader);
            while((groupName = bGroupReader.readLine()) != null) {
                groupArr[i] = new Group(groupName);
                i++;
            }
            groupReader.close();
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("Error,file 'group' not found.");
        }
        catch (IOException ioe) {
            System.out.println("Error,can't read file");
        }
    }
    //привязка студентов к группам
    public void bindStudentToGroup() {
        int rnd = 0;
        for(int i = 0;studentArr[i]!=null;i++){
            rnd = (int)(Math.random()* +3);
            studentArr[i].setGroup(groupArr[rnd]);
        }
    }
    //заполнение групп студентами
    public void bindGroupToStudent() {
        int Math = 0,Bio = 0,Eng = 0;
        for(int i = 0; i < 30;i++) {
            if (studentArr[i].getGroup().getTitle().equals("Math")) {
                groupArr[0].createStudent(Math, studentArr[i]);
                Math++;
            }
            if (studentArr[i].getGroup().getTitle().equals("Bio")) {
                groupArr[1].createStudent(Bio, studentArr[i]);
                Bio++;
            }
            if (studentArr[i].getGroup().getTitle().equals("Eng")) {
                groupArr[2].createStudent(Eng, studentArr[i]);
                Eng++;
            }
        }
    }

    //заполняем дневники студентов случайными оценками
    public void addRandomMark() {
        int random;
        for(int i = 0;studentArr[i]!=null;i++){
            for(int j = 0;j < 5;j++){
                random = 2 + (int) (Math.random() * 5);
                studentArr[i].setMark(random,j);
            }
        }
    }
    //перевод студента из группы в группу
    public void changeGroup(int idStudent,String nameGroup) {
        String currentGroup = studentArr[idStudent - 1].getGroup().getTitle();
        //записываем ссылку на группу в студента
        if (nameGroup.equals("Math"))
            studentArr[idStudent].setGroup(groupArr[0]);
        if (nameGroup.equals("Bio"))
            studentArr[idStudent].setGroup(groupArr[1]);
        if  (nameGroup.equals("Eng"))
            studentArr[idStudent].setGroup(groupArr[2]);

        //удаляем студента из группы
        if (currentGroup.equals("Math"))
            groupArr[0].deleteStudent(studentArr[idStudent - 1]);

        if (currentGroup.equals("Bio"))
            groupArr[1].deleteStudent(studentArr[idStudent - 1]);

        if (currentGroup.equals("Eng"))
            groupArr[2].deleteStudent(studentArr[idStudent - 1]);

        //добавляем студента в группу
        if (nameGroup.equals("Math"))
            groupArr[0].addStudent(studentArr[idStudent - 1]);
        if(nameGroup.equals("Bio"))
            groupArr[1].addStudent(studentArr[idStudent - 1]);
        if((nameGroup.equals("Eng")))
            groupArr[2].addStudent(studentArr[idStudent - 1]);

    }

    //отчисление студентов за неуспеваемость
    public void deleteStudent(int idStudent){
        if (studentArr[idStudent  - 1].getGroup().getTitle().equals("Math")) {
            groupArr[0].deleteStudent(studentArr[idStudent - 1]);
        }
        if (studentArr[idStudent - 1].getGroup().getTitle().equals("Bio")) {
            groupArr[1].deleteStudent(studentArr[idStudent - 1]);
        }
        if(studentArr[idStudent - 1].getGroup().getTitle().equals("Eng")) {
            groupArr[2].deleteStudent(studentArr[idStudent - 1]);
        }
        studentArr[idStudent-1]=null;
        int i=0,j=0;
        while(studentArr[i] != null)
            i++;
        j=i;
        i++;
        while(studentArr[i] != null)
            i++;
        studentArr[j] = studentArr[i-1];
        studentArr[i-1]=null;

    }
    //накопление статистики по успеваемости студентов и групп
    public void marksStatistic(){
        int i=0;
        System.out.println("Avg students marks");
        while(studentArr[i] != null) {
            System.out.println(studentArr[i].getAvgMark());
            i++;
        }
    }
    //вычисление среднего балла заданной группы
    void avgMarkInGroup(String nameOfGroup) {
        if (nameOfGroup.equals("Math")) {
            groupArr[0].avgMarkInGroup();
        } else if (nameOfGroup.equals("Bio")) {
            groupArr[1].avgMarkInGroup();
        } else if (nameOfGroup.equals("Eng")) {
            groupArr[2].avgMarkInGroup();
        } else
            System.out.println("Incorrect name group");
    }
    //Выбираем старосту в нужной группе
    void selHead(String nameOfGroup){
        if(nameOfGroup.equals("Math"))
            groupArr[0].selectionHead();
        else if(nameOfGroup.equals("Bio"))
            groupArr[1].selectionHead();
        else if(nameOfGroup.equals("Eng"))
            groupArr[2].selectionHead();
        else
            System.out.println("Incorrect name group");
    }
    //поиск по фио или id
    void findStudent(String name){
        for(int i=0;i<3;i++){
            groupArr[i].findStudentInGroup(name);
        }
    }
    void findStudent(int id){
        for(int i=0;i<3;i++){
            groupArr[i].findStudentInGroup(id);
        }
    }

    //вывод данных на консоль(имя студента - группа)
    public void printData() {
        int i=0;
        while(studentArr[i] != null){
            System.out.println(studentArr[i].getName() +"    "+ studentArr[i].getGroup().getTitle());
            i++;
        }
    }
    //вывод на консоль студентов(по группам)
    public void printDataP (){
        groupArr[0].printStudentInGroup();
        groupArr[1].printStudentInGroup();
        groupArr[2].printStudentInGroup();
    }
    //запись результата в файл
    void writeResult(String file){
        int i=0;
        try {
            Student temp;
            FileWriter fw = new FileWriter(file);
            while(studentArr[i]!=null){
                fw.write(studentArr[i].getName());
                fw.write("\t");
                fw.write(studentArr[i].getGroup().getTitle());
                fw.write("\t");
                fw.write(Double.toString(studentArr[i].getAvgMark()));
                fw.write("\n");
                i++;
            }
            fw.close();
        }
        catch (IOException ioe){
            System.out.println("Error,can't write in file");
        }


    }

}




