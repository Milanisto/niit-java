import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement (name = "group")
@XmlType (propOrder = {"title", "num", "head", "students"})
public class Group {
    private String title; // название группы
    private List<Student> students = new ArrayList<>();// массив ссылок на студентов
    @XmlElement
    private int num = 0; // кол-во студентов в группе
    @XmlElement
    private Student head; // ссылка на старосту группы

    public String getTitle() {
        return title;
    }

  public void setTitle(String title) {
        this.title = title;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
        setNum();
    }

    public int getNum() {
        return num;
    }

 public void setNum() {
        this.num = students.size();
    }

    // вычислить средний балл группы
    public double calculateAvScore() {
        double averageStudentsScore = 0;
        for (Student st : this.getStudents()) {
            averageStudentsScore += st.getAverageScore();
        }
        return averageStudentsScore / this.num;
    }

    public Student getHead() {
        return head;
    }

    //выборы старосты
    public void setHead() {
        double maxAvScore = 0.0;
        Student head = new Student();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getAverageScore() > maxAvScore) {
                maxAvScore = students.get(i).getAverageScore();
                head = students.get(i);
            }
        }
        System.out.println("Для группы " + this.title + " старостой выбран студент : id " + head.getId() + " " + head.getFio());
        this.head = head;
    }
}
