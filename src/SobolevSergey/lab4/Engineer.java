package staff;

// Engineer - инженер. Имеет ставку и оплату за час + бонусы от выполняемого проекта.
public class Engineer extends Employee implements WorkTime, Project{
    protected int base;
    protected String project;
    protected int projectCost;
    protected double degreeParticipation;

    public Engineer(int id, String name, int base, String project, int projectCost, double degreeParticipation) {
        super(id, name);
        this.base = base;
        this.project = project;
        this.projectCost = projectCost;
        this.degreeParticipation = degreeParticipation;
    }

    public int workTime(int worktime,int base){
        return worktime*base;
    }

    public double project (String project, int projectCost, double degreeParticipation) {
            return projectCost*degreeParticipation;
    }

    public void calc(){
        setPayment((int) (workTime(worktime,base) + project (project, projectCost, degreeParticipation)));
    }
}

// Programmer - инженер-программист.
class Programmer extends Engineer{
    public Programmer(int id, String name, int base, String project, int projectCost, double degreeParticipation) {
        super(id, name, base, project, projectCost, degreeParticipation);
    }
}

// Tester - инженер по тестированию.
final class Tester extends Engineer{
    public Tester(int id, String name, int base, String project, int projectCost, double degreeParticipation) {
        super(id, name, base, project, projectCost, degreeParticipation);
    }
}

// TeamLeader - ведущий программист.
final class TeamLeader extends Programmer implements Heading{

    protected  int numberSubordinates;
    protected int rateSubordinate;

    public TeamLeader(int id, String name, int base, String project, int projectCost, double degreeParticipation,
                      int numberSubordinates, int rateSubordinate) {

        super(id, name, base, project, projectCost, degreeParticipation);

        this.numberSubordinates = numberSubordinates;
        this.rateSubordinate = rateSubordinate;
    }

    public int heading (int numberSubordinates,int rateSubordinate) {
        return numberSubordinates*rateSubordinate;
    }

    public void calc(){
        setPayment((int) (workTime(worktime,base) + project (project, projectCost, degreeParticipation)
                + heading(numberSubordinates,rateSubordinate)));
    }
}