package staff;
import java.io.*;

public class Staff {

    static int M=100;                                             // MAX кол-во сотрудников в фирме
    static Employee [] staff = new Employee[M];                   // Employee - массив работников
    static int countEmployees = 0;                                // кол-во сотрудников

    public Staff() {
    }

    //загружаем первичный файл, создаем работников
    public static void CreatingStaff() {
        try {
            File file = new File("src\\main\\java\\staff\\dataFile.txt");
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);
            String line;
            int i = 0;
            while ((line = breader.readLine()) != null) {
                String[] arr = line.split("\t");
                int id = Integer.parseInt(arr[0]);                          // id сотрудника
                String name = arr[1];                                       // имя
                String position = arr[2];                                   // должность
                int base = Integer.parseInt(arr[3]);                        // базовая ставка
                String project = arr[4];                                    // в каком проекте учавствует
                int projectCost = Integer.parseInt(arr[5]);                 // стоимость проекта
                double degreeParticipation = Double.parseDouble(arr[6]);    // степень участия
                int numberSubordinates = Integer.parseInt(arr[7]);          // кол-во подчиненных
                int rateSubordinate = Integer.parseInt(arr[8]);             // ставка за подчиненного


                if (position.equals("Cleaner")) {
                    staff[i] = new Cleaner(id, name,base);

                } else if (position.equals("Driver")) {
                    staff[i] = new Driver(id, name, base);

                } else if (position.equals("Programmer")) {
                    staff[i] = new Programmer(id, name, base, project, projectCost, degreeParticipation);

                } else if (position.equals("TeamLeader")) {
                    staff[i] = new TeamLeader(id, name, base, project, projectCost, degreeParticipation,
                            numberSubordinates, rateSubordinate);

                } else if (position.equals("Tester")) {
                    staff[i] = new Tester(id, name, base, project, projectCost, degreeParticipation);

                } else if (position.equals("ProjectManager")) {
                    staff[i] = new ProjectManager(id, name, project, projectCost, degreeParticipation,
                            numberSubordinates, rateSubordinate);

                } else if (position.equals("SeniorManager")) {
                    staff[i] = new ProjectManager(id, name, project, projectCost, degreeParticipation,
                            numberSubordinates, rateSubordinate);
                }
                countEmployees= ++i;
            }
            reader.close();
        }catch(IOException ex) {
        System.out.println(ex.getMessage());
        }
    }
}