/*
Lab1_4. 
Программа, которая осуществляет свертку числовых диапазонов (обратная задаче к 3).
*/

import java.util.Scanner;

public class Lab1_4{
	
	public static int [] toIntArray (String str){  		
		StringBuilder result = new StringBuilder();	
		char[] arr_str = str.toCharArray();
		int counter = 0, j = 0, n_length = 0;
		for(int i = 0; i < arr_str.length; i++){		// Вычисляем общее количество цифр для создания массива
		    if (arr_str[i] == ',')
			    counter++;			
		}		
        int [] int_arr = new int [counter+1];		
        		
		for(int i = 0; i < arr_str.length; i++){
		    if (arr_str[i] != ','){
				result.append(arr_str[i]);	
				n_length++;						
			}	
			else{
				int_arr[j++] = Integer.parseInt(result.toString());
				result.delete(0,n_length+1);				
				n_length=0;
			} 
		} 
		int_arr[j] = Integer.parseInt(result.toString());
        return int_arr;		
	}
	
	public static String сonvolution (String str){
		StringBuilder convolList = new StringBuilder();    	 
		int [] arr = toIntArray(str);
		int count = 0, startRange = 0;			
		for(int i = 0; i < arr.length-1; i++){
            if((arr[i+1] - arr[i]) != 1){
				if(count > 1){					
					convolList.append(Integer.toString(startRange) + '-' + Integer.toString(startRange+count) + ',');					
					count = 0;
				}	
                else if(count == 1){					
					convolList.append(Integer.toString(startRange) + ',' + Integer.toString(startRange+count) + ',');					
					count = 0;
				}				
				else 
					convolList.append(Integer.toString(arr[i]) + ',');				
			}
			else{
				if(count == 0)
					startRange = arr[i];
				count++;
                if(	i == arr.length-2)
					convolList.append(Integer.toString(startRange) + '-' + Integer.toString(startRange+count));
			}	
            if (count == 0 && i == arr.length-2)
			    convolList.append(Integer.toString(arr[i+1]));			
		}
        return convolList.toString();	
	}
	
	public static void main (String[] args){		
		Scanner in = new Scanner(System.in);		
		System.out.print("Enter the list: \t");
		String line = сonvolution(in.nextLine());
		System.out.println("Convolution:\t\t" + line);		
	}
}