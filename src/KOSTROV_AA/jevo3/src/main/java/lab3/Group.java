package lab3;

/**
 * Created by Alexander on 28.03.2017.
 */
public class Group {
    private int id;
    private String title;
    Student[] students = new Student[40];
    private int num;
    private Student head;

    Group(String title, int id){
        this.setId(id);
        this.setTitle(title);
    }

    public int getId() {return id;}
    public Student getHead() {return head;}
    public String getTitle() {return title;}
    public int getNum() {return num;}

    public void setId(int id){ this.id = id; };
    public void setTitle(String title){ this.title = title; };
    public void setHead(Student head){this.head = head;}

    public void addStudent(Student student){
        try {
            int counter=0;
            student.setGroup(this);
            while(students[counter] instanceof Student){
                counter++;
            }
            students[counter] = student;
        } catch (ArrayIndexOutOfBoundsException ex){
            System.out.println(ex.getMessage());
        }

        }

    public Student search(int id){
        int counter = 0;
        try {
            while (students[counter].getId() != id)
                counter++;
        } catch (NullPointerException ex){

        }
        return students[counter];
    }



    public double getAverageMarks() {
        int counter =0;
        int sum = 0;
        try {
            for (Student i : students) {
                for (int ii = 0; ii < i.marks.length; ii++) {
                    if (i.marks[ii]>0){
                    counter++;
                    sum += i.marks[ii];}
                }
            }
        } catch(NullPointerException ex){
            //System.out.println(ex.getMessage());
        }
        return (float)sum/(float)counter;
    }
}
