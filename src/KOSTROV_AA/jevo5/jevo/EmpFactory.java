package jevo;


/**
 * Created by Alexander on 20.04.2017.
 */
public class EmpFactory {

    public Employee createEmployee(String position){
        Employee employee =null;

        switch (position) {
            case "Manager":
                employee = new Manager();
                break;
            case "ProjectManager":
                employee = new ProjectManager();
                break;
            case "SenjorManager":
                employee = new SeniorManager();
                break;
            case "Programmer":
                employee = new Programmer();
                break;
            case "Tester":
                employee = new Tester();
                break;
            case "TeamLeader":
                employee = new TeamLeader();
                break;
            case "Cleaner":
                employee = new Cleaner();
                break;
            case "Driver":
                employee = new Driver();
                break;

            default:
                employee = null;
                break;
        }

        return employee;
    }
}
