package jevo;

import java.util.Map;

/**
 * Created by Alexander on 16.04.2017.
 */
public interface Healing {
    public int healingPay();
}
